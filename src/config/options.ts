const embedColor = 0xeeaadb
const guildid = "481741000365178881"
const devMessage = "Developed by taken.lua"
const applicationsChannel = "776705352456470550"
const staffApplicationsChannel = "1039258641393520700"
const inactivityLogChannel = "829742524796239882"
const staffOtherChannel = "1082036748558803104"
const hypixelGuildID = "5a353a170cf2e529044f2935"
const onlineLogChannel = "1101144489306886226"
const botLogChannel = "1174403585149243472"
const guildLogChannel = "1183733282534326322"
const errorLogChannel = "1192476369850994788"
const moderationLogChannel = "1193329771795447818"
const devLogChannel = "1193688673632391280"
const waitingListChannel = "1145773618291298384"
const waitingListMessage = "1146027645415473193"

export {
    applicationsChannel,
    botLogChannel,
    devLogChannel,
    devMessage,
    embedColor,
    errorLogChannel,
    guildid,
    guildLogChannel,
    hypixelGuildID,
    inactivityLogChannel,
    moderationLogChannel,
    onlineLogChannel,
    staffApplicationsChannel,
    staffOtherChannel,
    waitingListChannel,
    waitingListMessage
}
