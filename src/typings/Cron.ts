import { TimeZones } from "./TimeZones"

interface ITimeFormat {
    seconds: "*" | number
    minutes: "*" | number
    hours: "*" | number
    dayOfMonth: "*" | number
    month: "*" | number
    dayOfWeek: "*" | number
}

export interface ICron {
    time: ITimeFormat
    execute: () => void
    onComplete?: null | undefined
    start?: boolean | null | undefined
    timeZone: TimeZones
}
