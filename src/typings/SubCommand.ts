import { ChatInputCommandInteraction } from "discord.js"
import { ExtendedClient } from "~/utils/Client"

export type SubCommand = (interaction: ChatInputCommandInteraction) => Promise<void>
export type SubCommmndClient = (interaction: ChatInputCommandInteraction, client: ExtendedClient) => Promise<void>
