import { ClientEvents } from "discord.js"

export type Event<E extends keyof ClientEvents> = (...args: ClientEvents[E]) => void
