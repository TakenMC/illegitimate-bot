import { MessageFlags } from "discord.js"
import { getWaitingLists, removeWaitingList } from "src/drizzle/functions"
import { hypixelGuildID } from "~/config/options"
import { IButton } from "~/typings"
import { getGuild, getIGN } from "~/utils/Hypixel"

export default {
    name: "waitinglistupdate",
    description: "Update the waiting list.",

    async execute({ interaction }) {
        await interaction.deferReply({ flags: MessageFlags.Ephemeral })

        const user = interaction.user
        const message = interaction.message
        const embed = message.embeds[0]
        const accepted = await getWaitingLists()

        for (let i = 0; i < accepted.length; i++) {
            const uuid = accepted[i].uuid
            const guild = await getGuild(uuid)

            if (guild && guild._id === hypixelGuildID) {
                await removeWaitingList({ uuid })
                continue
            }
        }

        const fields = []

        for (let i = 0; i < accepted.length; i++) {
            const ign = await getIGN(accepted[i].uuid)
            const timestamp = Math.floor(accepted[i].timestamp / 1000)

            fields.push({
                name: `${i + 1}. ${ign}`,
                value: `TS: <t:${timestamp}:R>`
            })
        }

        await message.edit({
            embeds: [{
                title: embed.title!,
                description: embed.description!,
                color: embed.color!,
                footer: {
                    text: "Last updated by " + user.username,
                    icon_url: user.avatarURL() || undefined
                },
                thumbnail: embed.thumbnail!,
                fields: fields,
                timestamp: new Date().toISOString()
            }]
        })

        await interaction.editReply("Updated the waiting list")
    }
} as IButton
