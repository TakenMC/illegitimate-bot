import { MessageFlags } from "discord.js"
import { IButton } from "~/typings"

export default {
    name: "inactiveapplicationdeny",
    description: "Denies an inactivity application.",

    async execute({ interaction }) {
        await interaction.reply({
            content: "This button is currently disabled.",
            flags: MessageFlags.Ephemeral
        })
    }
} as IButton
