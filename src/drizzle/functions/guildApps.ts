import { eq, or } from "drizzle-orm"
import db from "../db"
import { guildApps, InsertGuildApp } from "../schema"

type GuildAppDBData = Partial<Pick<InsertGuildApp, "userID" | "uuid">>

export async function addGuildApp(data: InsertGuildApp) {
    await db.insert(guildApps).values(data)
}

export async function getGuildApp(data: GuildAppDBData) {
    const userId = data.userID ?? ""
    const uuid = data.uuid ?? ""

    return await db.query.guildApps.findFirst({
        where: (({ userID: dbUserId, uuid: dbUuid }, { eq, or }) =>
            or(
                eq(dbUuid, uuid),
                eq(dbUserId, userId)
            ))
    })
}

export async function removeGuildApp(data: GuildAppDBData) {
    const userId = data.userID ?? ""
    const uuid = data.uuid ?? ""

    await db.delete(guildApps).where(or(
        eq(guildApps.userID, userId),
        eq(guildApps.uuid, uuid)
    ))
}
