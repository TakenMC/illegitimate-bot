import { eq, or } from "drizzle-orm"
import db from "../db"
import { InsertWaitingList, waitingLists } from "../schema"

type WaitingListDBData = Partial<Pick<InsertWaitingList, "userID" | "uuid">>

export async function addWaitingList(data: InsertWaitingList) {
    await db.insert(waitingLists).values(data)
}

export async function getWaitingList(data: WaitingListDBData) {
    const userId = data.userID ?? ""
    const uuid = data.uuid ?? ""

    return await db.query.waitingLists.findFirst({
        where: (({ userID: dbUserId, uuid: dbUuid }, { eq, or }) =>
            or(
                eq(dbUuid, uuid),
                eq(dbUserId, userId)
            ))
    })
}

export async function getWaitingLists() {
    return await db.query.waitingLists.findMany()
}

export async function removeWaitingList(data: WaitingListDBData) {
    const userId = data.userID ?? ""
    const uuid = data.uuid ?? ""

    await db.delete(waitingLists).where(or(
        eq(waitingLists.userID, userId),
        eq(waitingLists.uuid, uuid)
    ))
}
