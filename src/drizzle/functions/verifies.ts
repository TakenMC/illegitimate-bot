import { eq, or } from "drizzle-orm"
import db from "../db"
import { InsertVerify, verifies } from "../schema"

type VerifyDBData = Partial<Pick<InsertVerify, "userID" | "uuid">>

export async function addVerify(data: InsertVerify) {
    await db.insert(verifies).values(data)
}

export async function getVerify(data: VerifyDBData) {
    const userId = data.userID ?? ""
    const uuid = data.uuid ?? ""

    return await db.query.verifies.findFirst({
        where: (({ userID: dbUserId, uuid: dbUuid }, { eq, or }) =>
            or(
                eq(dbUuid, uuid),
                eq(dbUserId, userId)
            ))
    })
}

export async function getVerifies() {
    return await db.query.verifies.findMany()
}

export async function removeVerify(data: VerifyDBData) {
    const userId = data.userID ?? ""
    const uuid = data.uuid ?? ""

    await db.delete(verifies).where(or(
        eq(verifies.userID, userId),
        eq(verifies.uuid, uuid)
    ))
}
