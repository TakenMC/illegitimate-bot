import { eq, or } from "drizzle-orm"
import db from "../db"
import { InsertSetting, settings } from "../schema"

type SettingsDBData = Partial<Pick<InsertSetting, "name" | "value">>

export async function addSetting(data: InsertSetting) {
    await db.insert(settings).values(data)
}

export async function getSetting(data: SettingsDBData) {
    const name = data.name ?? ""
    const value = data.value ?? ""

    return await db.query.settings.findFirst({
        where: (({ name: dbName, value: dbValue }, { eq, or }) =>
            or(
                eq(dbName, name),
                eq(dbValue, value)
            ))
    })
}

export async function updateSetting(data: SettingsDBData) {
    const name = data.name ?? ""
    const value = data.value

    await db.update(settings).set({
        value: value
    }).where(eq(settings.name, name))
}

export async function removeSetting(data: SettingsDBData) {
    const name = data.name ?? ""
    const value = data.value ?? ""

    await db.delete(settings).where(or(
        eq(settings.name, name),
        eq(settings.value, value)
    ))
}
