import { eq, or } from "drizzle-orm"
import db from "../db"
import { InsertStaffApp, staffApps } from "../schema"

type StaffAppDBData = Partial<Pick<InsertStaffApp, "userID" | "uuid">>

export async function addStaffApp(data: InsertStaffApp) {
    await db.insert(staffApps).values(data)
}

export async function getStaffApp(data: StaffAppDBData) {
    const userId = data.userID ?? ""
    const uuid = data.uuid ?? ""

    return await db.query.staffApps.findFirst({
        where: (({ userID: dbUserId, uuid: dbUuid }, { eq, or }) =>
            or(
                eq(dbUuid, uuid),
                eq(dbUserId, userId)
            ))
    })
}

export async function removeStaffApp(data: StaffAppDBData) {
    const userId = data.userID ?? ""
    const uuid = data.uuid ?? ""

    await db.delete(staffApps).where(or(
        eq(staffApps.userID, userId),
        eq(staffApps.uuid, uuid)
    ))
}
