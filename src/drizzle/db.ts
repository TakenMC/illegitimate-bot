import { drizzle } from "drizzle-orm/postgres-js"
import env from "~/utils/Env"
import * as schema from "./schema"

const db = drizzle({
    connection: {
        url: env.prod.POSTGRESURI
    },
    schema: schema
})

export default db
