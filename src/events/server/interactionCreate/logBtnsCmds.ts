import { Event } from "~/typings"
import { log } from "~/utils/Logger"

const event: Event<"interactionCreate"> = (interaction) => {
    if (interaction.isChatInputCommand()) {
        let subcommand: string | null

        try {
            subcommand = interaction.options.getSubcommand()
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
        } catch (e) {
            subcommand = null
        }

        if (subcommand) {
            log(
                interaction.user.username + " ran " +
                    interaction.commandName + " " +
                    subcommand,
                "info",
                { type: "preset", color: "pink" }
            )
        } else {
            log(
                interaction.user.username + " ran " +
                    interaction.commandName,
                "info",
                { type: "preset", color: "pink" }
            )
        }
    }

    if (interaction.isButton()) {
        const customId = interaction.customId
        let btnId: string = ""

        if (customId.startsWith("tempbutton-")) {
            btnId = customId.split("-")[1]
            btnId = btnId.split("-")[0]
        }

        log(
            interaction.user.username + " clicked " + btnId,
            "info",
            { type: "preset", color: "pink" }
        )
        return
    }
}

export default event
