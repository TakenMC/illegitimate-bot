import { userMention } from "discord.js"
import { embedColor } from "~/config/options"
import { Event } from "~/typings"
import { dateTimeFormatter } from "~/utils/Functions/intlFormaters"
import logToChannel from "~/utils/Functions/logtochannel"

const event: Event<"guildMemberAdd"> = (member) => {
    if (process.env.NODE_ENV === "dev") return
    logToChannel("bot", {
        embeds: [{
            title: "New Member",
            description: userMention(member.id) + " has joined the server.\n" +
                "Account created: " + dateTimeFormatter(member.user.createdAt),
            color: embedColor,
            thumbnail: {
                url: member.user.avatarURL() || ""
            },
            footer: {
                text: "ID: " + member.id,
                icon_url: member.user.avatarURL() || undefined
            },
            timestamp: new Date().toISOString()
        }]
    })
}

export default event
