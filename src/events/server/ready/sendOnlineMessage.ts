import { embedColor } from "~/config/options"
import { Event } from "~/typings"
import logToChannel from "~/utils/Functions/logtochannel"

export const once = true
const event: Event<"ready"> = () => {
    if (process.env.NODE_ENV === "dev") return

    logToChannel("online", {
        embeds: [{
            description: "Bot is online!",
            color: embedColor
        }]
    })
}

export default event
