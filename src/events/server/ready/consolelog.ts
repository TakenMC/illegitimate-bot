import { Event } from "~/typings"
import { log } from "~/utils/Logger"

export const once = true
const event: Event<"ready"> = (client) => {
    log("Logged in as " + client.user!.tag + "!", "info", { type: "preset", color: "green" })
}

export default event
