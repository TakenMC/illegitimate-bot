import { Event } from "~/typings"

const event: Event<"voiceStateUpdate"> = (_o, n) => {
    const guild = n.guild

    if (!guild) return

    if (!n.channel) {
        guild.voiceStates.cache.delete(n.id)
    }
}

export default event
