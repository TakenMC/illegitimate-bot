import { GuildMember, InteractionContextType, SlashCommandBuilder } from "discord.js"
import { addVerify, getVerify } from "src/drizzle/functions"
import { devMessage, embedColor, hypixelGuildID } from "~/config/options"
import { ICommand, IGuildData, IPlayerData } from "~/typings"
import getGuildRank from "~/utils/Functions/guildrank"
import roleManage from "~/utils/Functions/rolesmanage"
import { getGuild, getHeadURL, getPlayer, getUUID } from "~/utils/Hypixel"

export default {
    name: "verify",
    description: "Verify yourself as a member of the server.",
    dev: false,
    public: true,

    data: new SlashCommandBuilder()
        .setName("verify")
        .setDescription("Verify yourself as a member of the server.")
        .addStringOption(option =>
            option
                .setName("ign")
                .setDescription("Your in-game name.")
                .setMinLength(3)
                .setMaxLength(16)
                .setRequired(true)
        )
        .setContexts(InteractionContextType.Guild),

    async execute({ interaction }) {
        await interaction.deferReply()

        const user = interaction.member! as GuildMember
        const ign = interaction.options.getString("ign")!

        const verifyData = await getVerify({ userID: user.id })
        if (verifyData) {
            interaction.editReply("You are already verified.\n" + "Try running /update to update your roles.")
            return
        }

        await interaction.editReply({
            embeds: [{
                description: "Fetching your uuid...",
                color: embedColor
            }]
        })

        const uuid = await getUUID(ign)
        if (!uuid) {
            interaction.editReply({
                embeds: [{
                    description: "<a:questionmark_pink:1130206038008803488> That player does not exist.",
                    color: embedColor
                }]
            })
            return
        }

        await interaction.editReply({
            embeds: [{
                description: "Fetching your player data...",
                color: embedColor
            }]
        })

        const head = getHeadURL(ign)
        const player = (await getPlayer(uuid)) as IPlayerData
        if (!player) {
            interaction.editReply({
                embeds: [{
                    description: "<a:questionmark_pink:1130206038008803488> That player hasn't played Hypixel before.",
                    color: embedColor
                }]
            })
            return
        }

        const username = user.user.username

        await interaction.editReply({
            embeds: [{
                description: "Checking your Discord tag...",
                color: embedColor
            }]
        })

        const linkedDiscord = player?.socialMedia?.links?.DISCORD
        if (!linkedDiscord) {
            interaction.editReply({
                embeds: [{
                    description: "<a:cross_a:1087808606897983539> There is no Discord account linked to `" + player.displayname + "`.\n\n" +
                        "**Please set your Discord tag on hypixel to `" + username + "` and try again.**",
                    color: embedColor
                }]
            })
            return
        }

        if (linkedDiscord !== username) {
            interaction.editReply({
                embeds: [{
                    description: "<a:cross_a:1087808606897983539> The Discord account linked to `" +
                        player.displayname + "` is currently `" + linkedDiscord + "`\n\n" +
                        "**Please set your Discord tag on hypixel to `" + username + "` and try again.**",
                    color: embedColor
                }]
            })
            return
        }

        await interaction.editReply({
            embeds: [{
                description: "Fetching your guild data...",
                color: embedColor
            }]
        })

        const guild = (await getGuild(uuid)) as IGuildData | null
        let guildID: string | null
        if (!guild) {
            guildID = null
        } else {
            guildID = guild._id
        }

        if (guildID === hypixelGuildID) {
            const GuildMembers = guild!.members
            const guildRank = GuildMembers.find(member => member.uuid === player.uuid)!.rank

            const rank = getGuildRank(guildRank)
            if (rank) {
                await user.roles.add(rank.rolesToAdd, "Verification")
            }
        }

        await user.roles.add(roleManage("default").rolesToAdd, "Verification")
        await user.setNickname(player.displayname!, "Verification").catch(() => {
            // Do nothing
        })

        await addVerify({
            userID: user.id,
            uuid: uuid
        })

        await interaction.editReply({
            embeds: [{
                title: interaction.guild!.name,
                description: "You have successfully verified `" + username + "` with the account `" + player.displayname + "`.",
                color: embedColor,
                thumbnail: {
                    url: head!
                },
                footer: {
                    icon_url: interaction.guild!.iconURL() || undefined,
                    text: interaction.guild!.name + " | " + devMessage
                }
            }]
        })
    }
} as ICommand
