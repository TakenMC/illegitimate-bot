import { ChannelType, GuildMember } from "discord.js"
import { getVerifies } from "src/drizzle/functions"
import { embedColor, hypixelGuildID } from "~/config/options"
import { IGuildData, SubCommand } from "~/typings"
import env from "~/utils/Env"
import getGuildRank from "~/utils/Functions/guildrank"
import roleManage from "~/utils/Functions/rolesmanage"
import { getGuild, getIGN } from "~/utils/Hypixel"
import { log } from "~/utils/Logger"

const cmd: SubCommand = async (interaction) => {
    await interaction.deferReply()

    const discordMember = interaction.member as GuildMember
    const channel = interaction.channel

    if (channel?.type !== ChannelType.GuildText) {
        await interaction.editReply({
            embeds: [{
                description: "This command can only be used in a server.",
                color: embedColor
            }]
        })
        return
    }

    if (discordMember.user.id !== env.prod.DEV) {
        await interaction.editReply({
            embeds: [{
                description: "You do not have permission to use this command.",
                color: embedColor
            }]
        })
        return
    }

    const guildMembers = await interaction.guild!.members.fetch().then(
        members =>
            members.map(member => {
                return {
                    id: member.id,
                    member: member
                }
            })
    )

    const guildData = (await getGuild(hypixelGuildID, "id")) as IGuildData

    const hypixelGuildMembers = guildData.members.map(gmember => {
        return {
            uuid: gmember.uuid,
            rank: gmember.rank
        }
    })
    const guildMemberIDs = hypixelGuildMembers.map(gmember => gmember.uuid)

    const verifiedUsers = await getVerifies()

    await interaction.editReply({
        embeds: [{
            description: `Updating roles for ${guildMembers.length} members...`,
            color: embedColor
        }]
    })

    let i = 1
    for (const gmember of guildMembers) {
        const memberData = verifiedUsers.find(user => user.userID === gmember.id)

        log(`Updating ${gmember.member.user.username} [${i}/${guildMembers.length}]`, "info", { type: "preset", color: "green" })
        i++

        if (!memberData) {
            if (gmember.member.user.bot) {
                log(` Skipped bot [${gmember.member.user.username}]`, "info", { type: "preset", color: "lavender" })
                continue
            }
            const roles = roleManage("defaultnoverify")
            await gmember.member.roles.remove(roles.rolesToRemove, "Updating all discord members")
            await gmember.member.roles.add(roles.rolesToAdd, "Updating all discord members")
            await gmember.member.setNickname(`${gmember.member.user.username} (X)`, "Updating all discord members").catch(() => {
                // Do nothing
            })
            log(`${gmember.member.user.username} [X]`, "info", { type: "preset", color: "lavender" })
        } else {
            const uuid = memberData.uuid
            const ign = await getIGN(uuid)

            if (!guildMemberIDs.includes(memberData?.uuid)) {
                const roles = roleManage("default")
                await gmember.member.roles.remove(roles.rolesToRemove, "Updating all discord members")
                await gmember.member.roles.add(roles.rolesToAdd, "Updating all discord members")
                log(`${gmember.member.user.username} [Default]`, "info", { type: "preset", color: "lavender" })
            } else if (guildMemberIDs.includes(memberData!.uuid)) {
                const guildMemberRank = hypixelGuildMembers.find(gmember => gmember.uuid === memberData!.uuid)!.rank
                log(" Updating roles for " + gmember.member.user.username, "info", { type: "preset", color: "lavender" })

                const rank = getGuildRank(guildMemberRank)
                if (rank) {
                    await gmember.member.roles.remove(rank.rolesToRemove, "Updating all discord members")
                    await gmember.member.roles.add(rank.rolesToAdd, "Updating all discord members")
                }
            }
            await gmember.member.setNickname(ign, "Updating all discord members").catch(() => {
                // Do nothing
            })
        }
    }

    log("Successfully updated all roles.", "info")

    await channel.send({
        embeds: [{
            description: "Successfully updated all roles.",
            color: embedColor
        }]
    })
}

export default cmd
