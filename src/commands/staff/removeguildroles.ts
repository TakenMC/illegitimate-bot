import { GuildMember } from "discord.js"
import { getVerifies } from "src/drizzle/functions"
import { embedColor, hypixelGuildID } from "~/config/options"
import { IGuildData, SubCommand } from "~/typings"
import env from "~/utils/Env"
import roleManage from "~/utils/Functions/rolesmanage"
import { getGuild } from "~/utils/Hypixel"

const cmd: SubCommand = async (interaction) => {
    await interaction.deferReply()

    const discordMember = interaction.member as GuildMember

    if (discordMember.user.id !== env.prod.DEV) {
        await interaction.editReply({
            embeds: [{
                description: "You do not have permission to use this command.",
                color: embedColor
            }]
        })
        return
    }

    const guildMembers = await interaction.guild!.members.fetch().then(
        members =>
            members.map(member => {
                return {
                    id: member.id,
                    member: member
                }
            })
    )

    const guildData = (await getGuild(hypixelGuildID, "id")) as IGuildData

    const hypixelGuildMembers = guildData.members.map(gmember => gmember.uuid)

    const verifiedUsers = await getVerifies()

    for (const gmember of guildMembers) {
        const gmemberuuid = verifiedUsers.find(user => user.userID === gmember.id)?.uuid
        const roles = roleManage("default")

        if (!gmemberuuid) {
            await gmember.member.roles.remove(roles.rolesToRemove)
            continue
        }

        if (!hypixelGuildMembers.includes(gmemberuuid)) {
            await gmember.member.roles.remove(roles.rolesToRemove)
            continue
        }
    }
}

export default cmd
