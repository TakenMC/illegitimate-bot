import { InteractionContextType, SlashCommandBuilder } from "discord.js"
import { devMessage, embedColor } from "~/config/options"
import { ICommand } from "~/typings"
import { formatUuid, getHeadURL, getIGN, getUUID } from "~/utils/Hypixel"

export default {
    name: "uuid",
    description: "Get a player's UUID",
    dev: false,
    public: true,

    data: new SlashCommandBuilder()
        .setName("uuid")
        .setDescription("Get a player's UUID")
        .addStringOption(option =>
            option
                .setName("ign")
                .setDescription("Player's name")
                .setMinLength(3)
                .setMaxLength(16)
                .setRequired(true)
        )
        .setContexts(InteractionContextType.Guild),

    async execute({ interaction }) {
        await interaction.deferReply()

        const ign = interaction.options.getString("ign")!
        const uuid = (await getUUID(ign)) as string

        if (!uuid) {
            interaction.editReply({
                embeds: [{
                    description: "<a:questionmark_pink:1130206038008803488> That player does not exist.",
                    color: embedColor
                }]
            })
            return
        }

        const formattedUuid = formatUuid(uuid)
        const newIgn = await getIGN(uuid) as string
        const head = getHeadURL(ign)

        await interaction.editReply({
            embeds: [{
                title: newIgn,
                description: "**UUID:** `" + uuid + "`\n" +
                    "**Formatted UUID:** `" + formattedUuid + "`",
                color: embedColor,
                thumbnail: {
                    url: head!
                },
                footer: {
                    text: interaction.guild!.name + " | " + devMessage,
                    icon_url: interaction.guild!.iconURL() || undefined
                }
            }]
        })
    }
} as ICommand
