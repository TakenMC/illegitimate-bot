import { devMessage, embedColor } from "~/config/options"
import { IGuildData, SubCommand } from "~/typings"
import { dateTimeFormatter, numberFormatter } from "~/utils/Functions/intlFormaters"
import { getGuild, getIGN, getPlayer, getUUID, guildLevel } from "~/utils/Hypixel"

const cmd: SubCommand = async (interaction) => {
    await interaction.deferReply()

    const query = interaction.options.getString("query")!
    const type = interaction.options.getString("type") || "ign"
    let guild: IGuildData | null

    if (type === "ign") {
        await interaction.editReply({
            embeds: [{
                description: "Fetching your uuid...",
                color: embedColor
            }]
        })

        const uuid = await getUUID(query)
        if (!uuid) {
            interaction.editReply({
                embeds: [{
                    description: "<a:questionmark_pink:1130206038008803488> That player does not exist.",
                    color: embedColor
                }]
            })
            return
        }

        await interaction.editReply({
            embeds: [{
                description: "Fetching your player data...",
                color: embedColor
            }]
        })

        const player = await getPlayer(uuid)
        if (!player) {
            interaction.editReply({
                embeds: [{
                    description: "<a:questionmark_pink:1130206038008803488> That player hasn't played Hypixel before.",
                    color: embedColor
                }]
            })
            return
        }

        await interaction.editReply({
            embeds: [{
                description: "Fetching your guild data...",
                color: embedColor
            }]
        })

        guild = await getGuild(uuid, "player")
        if (!guild) {
            interaction.editReply({
                embeds: [{
                    description: "That player is not in a guild!",
                    color: embedColor
                }]
            })
            return
        }
    } else if (type === "name") {
        await interaction.editReply({
            embeds: [{
                description: "Fetching your guild data...",
                color: embedColor
            }]
        })

        guild = await getGuild(query, "name")
        if (!guild) {
            interaction.editReply({
                embeds: [{
                    description: "That guild doesn't exist!",
                    color: embedColor
                }]
            })
            return
        }
    } else if (type === "id") {
        await interaction.editReply({
            embeds: [{
                description: "Fetching your guild data...",
                color: embedColor
            }]
        })

        guild = await getGuild(query, "id")
        if (!guild) {
            interaction.editReply({
                embeds: [{
                    description: "That guild doesn't exist!",
                    color: embedColor
                }]
            })
            return
        }
    }

    const guildName = guild!.name
    const guildCreatedMS = guild!.created
    const guildCreated = new Date(guildCreatedMS)
    const guildTag = guild!.tag
    const guildExpUnformatted = guild!.exp
    const guildExp = numberFormatter(guildExpUnformatted)
    const guildLvl = guildLevel(guildExpUnformatted)
    const guildMembers = guild!.members
    const guildCreatedTime = dateTimeFormatter(guildCreated)

    const guildOwner = guildMembers.find(m => m.rank === "Guild Master")!.uuid
    const guildOwnerName = await getIGN(guildOwner)
    const guildRanksUnsorted = guild!.ranks.sort((a, b) => b.priority - a.priority)
    const guildRanks = guildRanksUnsorted.map(r => "**➺ " + r.name + "** `[" + r.tag + "]`").join("\n")

    const allGuildMembersWeeklyXP = guildMembers.map(member => member.expHistory)
    const guildMembersWeeklyXP = allGuildMembersWeeklyXP.map(member => {
        return Object.values(member).reduce((a, b) => a + b, 0)
    })

    const totalGuildMembersWeeklyXPUnformatted = guildMembersWeeklyXP.reduce((a, b) => a + b, 0)
    const totalGuildMembersWeeklyXP = numberFormatter(totalGuildMembersWeeklyXPUnformatted)

    const averageGuildMembersWeeklyXPUnformatted = Math.round(totalGuildMembersWeeklyXPUnformatted / 7)
    const averageGuildMembersWeeklyXP = numberFormatter(averageGuildMembersWeeklyXPUnformatted)

    await interaction.editReply({
        embeds: [{
            title: "**Info on** " + guildName,
            description: "**Guild Name: **`" + guildName + "`\n" +
                "**Guild Tag: **`" + guildTag + "`\n" +
                "**Guild Level: **`" + guildLvl + "`\n" +
                "**Guild Owner: **`" + guildOwnerName + "`",
            fields: [
                {
                    name: "**Guild Ranks**",
                    value: guildRanks
                },
                {
                    name: "**GEXP**",
                    value: "**➺ Total weekly GEXP:** `" + totalGuildMembersWeeklyXP + "`\n" +
                        "**➺ Daily avarage:** `" + averageGuildMembersWeeklyXP + "`\n" +
                        "**➺ Total GEXP:** `" + guildExp + "`"
                },
                {
                    name: "**Guild Created**",
                    value: "**➺ **`" + guildCreatedTime + "`"
                }
            ],
            color: embedColor,
            footer: {
                text: interaction.guild!.name + " | " + devMessage,
                icon_url: interaction.guild!.iconURL() || undefined
            }
        }]
    })
}

export default cmd
