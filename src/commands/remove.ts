import { GuildMember, InteractionContextType, PermissionFlagsBits, SlashCommandBuilder, userMention } from "discord.js"
import { getWaitingList, removeWaitingList } from "src/drizzle/functions"
import { devMessage, embedColor } from "~/config/options"
import { waitingListRole } from "~/config/roles"
import { ICommand } from "~/typings"
import logToChannel from "~/utils/Functions/logtochannel"

export default {
    name: "remove",
    description: "Remove a person on the waiting list.",
    dev: false,
    public: false,

    data: new SlashCommandBuilder()
        .setName("remove")
        .setDescription("Remove a person on the waiting list.")
        .addUserOption(option =>
            option
                .setName("user")
                .setDescription("The user to remove.")
                .setRequired(true)
        )
        .addStringOption(option =>
            option
                .setName("reason")
                .setDescription("The reason for removing the user.")
        )
        .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
        .setContexts(InteractionContextType.Guild),

    async execute({ interaction }) {
        await interaction.deferReply()

        const member = interaction.options.getMember("user") as GuildMember
        const reason = interaction.options.getString("reason") ?? "No reason provided."
        const mod = interaction.user!
        const waiting = await getWaitingList({ userID: member.user.id })

        if (!waiting) {
            await interaction.editReply({
                embeds: [{
                    description: userMention(member.user.id) + " is not on the waiting list.",
                    color: embedColor
                }]
            })
            return
        }

        await removeWaitingList({ userID: member.user.id })
        await member.roles.remove(waitingListRole, "Removed from waiting list.")

        await logToChannel("mod", {
            embeds: [{
                author: {
                    name: mod.username,
                    icon_url: mod.avatarURL() || undefined
                },
                title: "Waiting List - Remove User",
                description: `
                **User:** ${userMention(member.user.id)}
                **Reason:** ${reason}
                **Mod:** ${userMention(mod.id)}
                `.removeIndents(),
                color: embedColor,
                thumbnail: {
                    url: mod.avatarURL() || ""
                },
                footer: {
                    icon_url: member.avatarURL() || undefined,
                    text: "ID: " + member.user.id
                },
                timestamp: new Date().toISOString()
            }]
        })

        await interaction.editReply({
            embeds: [{
                title: "Waiting List - Remove User",
                description: "**User:** " + userMention(member.user.id) + "\n" +
                    "**Reason:** `" + reason + "`",
                color: embedColor,
                footer: {
                    text: interaction.guild!.name + " | " + devMessage,
                    icon_url: interaction.guild!.iconURL() || undefined
                }
            }]
        })
    }
} as ICommand
