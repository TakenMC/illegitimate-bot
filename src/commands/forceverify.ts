import { GuildMember, InteractionContextType, PermissionFlagsBits, SlashCommandBuilder, userMention } from "discord.js"
import { addVerify, getVerify } from "src/drizzle/functions"
import { devMessage, embedColor, hypixelGuildID } from "~/config/options"
import { ICommand } from "~/typings"
import getGuildRank from "~/utils/Functions/guildrank"
import logToChannel from "~/utils/Functions/logtochannel"
import roleManage from "~/utils/Functions/rolesmanage"
import { getGuild, getHeadURL, getPlayer, getUUID } from "~/utils/Hypixel"

export default {
    name: "forceverify",
    description: "Force verify a user.",
    dev: false,
    public: false,

    data: new SlashCommandBuilder()
        .setName("forceverify")
        .setDescription("Force verify a user.")
        .addUserOption(option =>
            option
                .setName("user")
                .setDescription("The user to force verify.")
                .setRequired(true)
        )
        .addStringOption(option =>
            option
                .setName("ign")
                .setDescription("The user's in-game name.")
                .setRequired(true)
        )
        .setDefaultMemberPermissions(PermissionFlagsBits.Administrator)
        .setContexts(InteractionContextType.Guild),

    async execute({ interaction }) {
        await interaction.deferReply()

        const user = interaction.options.getMember("user")! as GuildMember
        const ign = interaction.options.getString("ign")!
        const mod = interaction.user

        const verifyData = await getVerify({ userID: user.id })
        if (verifyData) {
            interaction.editReply("That user is already verified.")
            return
        }

        const username = user.user.username
        const modName = mod.username

        await interaction.editReply({
            embeds: [{
                description: "Fetching their uuid...",
                color: embedColor
            }]
        })

        const uuid = await getUUID(ign)
        if (!uuid) {
            interaction.editReply({
                embeds: [{
                    description: "<a:questionmark_pink:1130206038008803488> That player doesn't exist.",
                    color: embedColor
                }]
            })
            return
        }

        await interaction.editReply({
            embeds: [{
                description: "Fetching their player data...",
                color: embedColor
            }]
        })

        const player = await getPlayer(uuid)
        if (!player) {
            interaction.editReply({
                embeds: [{
                    description: "<a:questionmark_pink:1130206038008803488> That player hasn't played Hypixel before.",
                    color: embedColor
                }]
            })
            return
        }

        await interaction.editReply({
            embeds: [{
                description: "Fetching their guild data...",
                color: embedColor
            }]
        })

        const guild = await getGuild(uuid)
        let responseGuildID: string | null
        if (!guild) {
            responseGuildID = null
        } else {
            responseGuildID = guild._id
        }

        const head = getHeadURL(ign)
        if (responseGuildID === hypixelGuildID) {
            const GuildMembers = guild!.members
            const guildRank = GuildMembers.find(member => member.uuid === player.uuid)!.rank

            const rank = getGuildRank(guildRank)
            if (rank) {
                await user.roles.add(rank.rolesToAdd, "User was force verified by " + modName)
            }
        }

        await user.roles.add(roleManage("default").rolesToAdd, "User was force verified by " + modName)
        await user.setNickname(player.displayname!, "User was force verified by " + modName).catch(() => {
            // Do nothing
        })

        await addVerify({
            userID: user.user.id,
            uuid: uuid
        })

        await logToChannel("mod", {
            embeds: [{
                author: {
                    name: modName,
                    icon_url: mod.avatarURL() || undefined
                },
                title: "Force Verified",
                description: `
                **User:** ${userMention(user.id)}
                **Mod:** ${userMention(mod.id)}
                **IGN:** \`${player.displayname}\`
                **UUID:** \`${uuid}\`
                `.removeIndents(),
                color: embedColor,
                thumbnail: {
                    url: mod.avatarURL() || ""
                },
                footer: {
                    icon_url: user.user.avatarURL() || undefined,
                    text: "ID: " + user.user.id
                },
                timestamp: new Date().toISOString()
            }]
        })

        await interaction.editReply({
            embeds: [{
                title: interaction.guild!.name,
                description: "You have successfully force verified `" + username + "` with the account `" + player.displayname + "`.",
                color: embedColor,
                thumbnail: {
                    url: head!
                },
                footer: {
                    icon_url: interaction.guild!.iconURL() || undefined,
                    text: interaction.guild!.name + " | " + devMessage
                }
            }]
        })
    }
} as ICommand
