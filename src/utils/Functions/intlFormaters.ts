export function numberFormatter(d: number): string {
    return new Intl.NumberFormat("en-US").format(d)
}

export function dateTimeFormatter(d: Date): string {
    return new Intl.DateTimeFormat("hr-HR", {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        hour: "numeric",
        minute: "numeric",
        second: "numeric"
    }).format(d)
}

export function logTimeFormatter(d: Date): string {
    return new Intl.DateTimeFormat("hr-HR", {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        hour: "numeric",
        minute: "numeric"
    }).format(d)
}
