import roleManage from "./rolesmanage"

export default function getGuildRank(rank: string) {
    if (rank === "Guild Master") {
        const roles = roleManage("gm")
        return {
            rolesToRemove: roles.rolesToRemove,
            rolesToAdd: roles.rolesToAdd,
            rank: "Guild Master"
        }
    }

    if (rank === "Leader") {
        const roles = roleManage("leader")
        return {
            rolesToRemove: roles.rolesToRemove,
            rolesToAdd: roles.rolesToAdd,
            rank: "Leader"
        }
    }

    if (rank === "Staff") {
        const roles = roleManage("staff")
        return {
            rolesToRemove: roles.rolesToRemove,
            rolesToAdd: roles.rolesToAdd,
            rank: "Staff"
        }
    }

    if (rank === "Beast") {
        const roles = roleManage("beast")
        return {
            rolesToRemove: roles.rolesToRemove,
            rolesToAdd: roles.rolesToAdd,
            rank: "Beast"
        }
    }

    if (rank === "Elite") {
        const roles = roleManage("elite")
        return {
            rolesToRemove: roles.rolesToRemove,
            rolesToAdd: roles.rolesToAdd,
            rank: "Elite"
        }
    }

    if (rank === "Member") {
        const roles = roleManage("member")
        return {
            rolesToRemove: roles.rolesToRemove,
            rolesToAdd: roles.rolesToAdd,
            rank: "Member"
        }
    }
}
