import { beast, defaultMember, elite, gm, guildRole, guildStaff, leader, member, staff, verifyTick } from "~/config/roles"
const roles = [
    gm,
    leader,
    staff,
    beast,
    elite,
    member,
    guildStaff,
    guildRole
]

type RoleType =
    | "gm"
    | "leader"
    | "staff"
    | "beast"
    | "elite"
    | "member"
    | "default"
    | "defaultnoverify"
    | "all"

export default function roleManage(role: RoleType): { rolesToRemove: string[], rolesToAdd: string[] } {
    if (role === "gm") {
        const rolesToRemove = roles.filter(role => role !== gm && role !== guildStaff && role !== guildRole)
        const rolesToAdd = [gm, guildStaff, guildRole, verifyTick]
        return { rolesToRemove, rolesToAdd }
    }

    if (role === "leader") {
        const rolesToRemove = roles.filter(role => role !== leader && role !== guildStaff && role !== guildRole)
        const rolesToAdd = [leader, guildStaff, guildRole, verifyTick]
        return { rolesToRemove, rolesToAdd }
    }

    if (role === "staff") {
        const rolesToRemove = roles.filter(role => role !== staff && role !== guildStaff && role !== guildRole)
        const rolesToAdd = [staff, guildStaff, guildRole, verifyTick]
        return { rolesToRemove, rolesToAdd }
    }

    if (role === "beast") {
        const rolesToRemove = roles.filter(role => role !== beast && role !== guildRole)
        const rolesToAdd = [beast, guildRole, verifyTick]
        return { rolesToRemove, rolesToAdd }
    }

    if (role === "elite") {
        const rolesToRemove = roles.filter(role => role !== elite && role !== guildRole)
        const rolesToAdd = [elite, guildRole, verifyTick]
        return { rolesToRemove, rolesToAdd }
    }

    if (role === "member") {
        const rolesToRemove = roles.filter(role => role !== member && role !== guildRole)
        const rolesToAdd = [member, guildRole, verifyTick]
        return { rolesToRemove, rolesToAdd }
    }

    if (role === "default") {
        const rolesToRemove = roles
        const rolesToAdd = [defaultMember, verifyTick]
        return { rolesToRemove, rolesToAdd }
    }

    if (role === "defaultnoverify") {
        const rolesToRemove = roles
        const rolesToAdd = [defaultMember]
        return { rolesToRemove, rolesToAdd }
    }

    if (role === "all") {
        const rolesToRemove = roles
        rolesToRemove.push(verifyTick)
        rolesToRemove.push(defaultMember)

        return { rolesToRemove, rolesToAdd: [] }
    }

    return { rolesToRemove: [], rolesToAdd: [] }
}
