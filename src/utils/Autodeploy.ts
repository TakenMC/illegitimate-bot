import { RESTPostAPIChatInputApplicationCommandsJSONBody, RESTPostAPIContextMenuApplicationCommandsJSONBody } from "discord.js"
import fs from "fs"
import { ICommand, IContextMenu } from "~/typings"
import { ExtendedClient } from "./Client"
import env from "./Env"
import { log } from "./Logger"
type FileType = "js" | "ts"

export default async function autoDeployCommands(fileType: FileType, client: ExtendedClient) {
    type CommandsType = RESTPostAPIChatInputApplicationCommandsJSONBody | RESTPostAPIContextMenuApplicationCommandsJSONBody
    const commands: CommandsType[] = []
    let commandFiles: string[] = []
    let contentMenuCommands: string[] = []

    if (fileType === "js") {
        commandFiles = fs.readdirSync("./dist/commands/").filter(file => file.endsWith(fileType))
        contentMenuCommands = fs.readdirSync("./dist/commands-contextmenu/").filter(file => file.endsWith(fileType))
    } else if (fileType === "ts") {
        commandFiles = fs.readdirSync("./src/commands/").filter(file => file.endsWith(fileType))
        contentMenuCommands = fs.readdirSync("./src/commands-contextmenu/").filter(file => file.endsWith(fileType))
    }

    for (const file of commandFiles) {
        const { default: command } = await import(`../commands/${file}`) as { default: ICommand }
        if (command.dev) {
            commands.push(command.data.toJSON())
        }
    }

    for (const file of contentMenuCommands) {
        const { default: command } = await import(`../commands-contextmenu/${file}`) as { default: IContextMenu }
        if (command.dev) {
            commands.push(command.data.toJSON())
        }
    }

    const commandData = commands.map(command => {
        return {
            name: command.name,
            options: command.options?.map(option => {
                return {
                    name: option.name,
                    description: option.description,
                    type: option.type
                }
            }) ?? [],
            defaultPermission: command.default_member_permissions ?? null
        }
    }).sort((a, b) => a.name > b.name ? 1 : -1)

    client.on("ready", async (c) => {
        const guildclient = c.guilds.cache.get(env.dev.GUILDID)!
        const currentCommands = await guildclient.commands.fetch()
        if (!currentCommands) return

        const currentCommandsData = currentCommands.map(command => {
            return {
                name: command.name,
                options: command.options?.map(option => {
                    return {
                        name: option.name,
                        description: option.description,
                        type: option.type
                    }
                }),
                defaultPermission: command.defaultMemberPermissions
            }
        }).sort((a, b) => a.name > b.name ? 1 : -1)

        const nc = commands.map(cmd => {
            return " " + cmd.name
        })
        // }).join("\n")

        if (JSON.stringify(commandData) === JSON.stringify(currentCommandsData)) {
            log("Commands are up to date.", "info", { type: "preset", color: "green" })
            // log(nc, "info", { type: "preset", color: "lavender" })
            nc.forEach(c => log(c, "info", { type: "preset", color: "lavender" }))
        } else {
            log("Commands are not up to date.", "info", { type: "preset", color: "red" })

            if (currentCommands.size === 0) {
                for (const cmd of commands) {
                    await guildclient.commands.create(cmd)
                }
                // log(nc, "info", { type: "preset", color: "lavender" })
                nc.forEach(c => log(c, "info", { type: "preset", color: "lavender" }))
                log("All commands were registered.", "info", { type: "preset", color: "green" })
                return
            }

            for (const cmd of currentCommandsData) {
                if (!commandData.find(c => c.name === cmd.name)) {
                    await guildclient.commands.delete(currentCommands.find(c => c.name === cmd.name)!.id)
                    log(" " + cmd.name + " was unregistered.", "info", { type: "preset", color: "red" })
                }
            }

            for (const cmd of commandData) {
                if (!currentCommandsData.find(c => c.name === cmd.name)) {
                    await guildclient.commands.create(commands.find(c => c.name === cmd.name)!)
                    log(" " + cmd.name + " was registered.", "info", { type: "preset", color: "lavender" })
                }
            }

            for (const cmd of commandData) {
                if (!currentCommandsData.find(c => c.name === cmd.name)) continue

                const currentCommand = currentCommands.find(c => c.name === cmd.name)!
                const currentCommandData = currentCommandsData.find(c => c.name === cmd.name)!

                if (JSON.stringify(cmd) !== JSON.stringify(currentCommandData)) {
                    await guildclient.commands.edit(currentCommand.id, cmd)
                    log(" " + cmd.name + " was updated.", "info", { type: "preset", color: "lavender" })
                }
            }

            log("-------------", "info", { type: "preset", color: "lavender" })
            // log(nc, "info", { type: "preset", color: "lavender" })
            nc.forEach(c => log(c, "info", { type: "preset", color: "lavender" }))
        }
    })
}
