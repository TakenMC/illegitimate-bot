import { Client, Collection, GatewayIntentBits, Partials } from "discord.js"
import { IAutocomplete, IButton, ICommand, IContextMenu, IModal } from "~/typings"
import autoDeployCommands from "./Autodeploy"
import env from "./Env"
import { log } from "./Logger"

export class ExtendedClient extends Client {
    commands: Collection<string, ICommand> = new Collection()
    contextmenus: Collection<string, IContextMenu> = new Collection()
    buttons: Collection<string, IButton> = new Collection()
    modals: Collection<string, IModal> = new Collection()
    autocomplete: Collection<string, IAutocomplete> = new Collection()

    constructor() {
        super({
            intents: [
                GatewayIntentBits.Guilds,
                GatewayIntentBits.GuildMessages,
                GatewayIntentBits.GuildMembers,
                GatewayIntentBits.MessageContent,
                GatewayIntentBits.DirectMessages,
                GatewayIntentBits.GuildVoiceStates
            ],
            partials: [
                Partials.GuildMember,
                Partials.User,
                Partials.Message,
                Partials.Channel
            ]
        })
    }

    async start() {
        let token: string
        if (process.env.NODE_ENV === "dev" && process.env.TYPESCRIPT === "true") {
            log("Running in development mode. [tsx]", "info", { type: "preset", color: "lavender" })
            token = env.dev.DEVTOKEN
            autoDeployCommands("ts", this)
        } else if (process.env.NODE_ENV === "dev" && !process.env.TYPESCRIPT) {
            log("Running in development mode.", "info", { type: "preset", color: "lavender" })
            token = env.dev.DEVTOKEN
            autoDeployCommands("js", this)
        } else {
            log("Running in production mode.", "info", { type: "preset", color: "green" })
            token = env.prod.TOKEN
        }

        this.login(token)
    }
}
