import { Redis } from "ioredis"
import { ExtendedClient as Client } from "~/utils/Client"
import env from "~/utils/Env"
import loadAllEvents from "./Events/loadevents"
import { log } from "./Logger"

const client = new Client()
const redis = new Redis(env.prod.REDISURI)

let ft: "js" | "ts"
if (process.env.NODE_ENV === "dev" && process.env.TYPESCRIPT === "true") {
    ft = "ts"
} else {
    ft = "js"
}

class Illegitimate {
    async start() {
        await loadAllEvents(client, ft)
        await client.start()
        await this.databases()
        this.loadMethods()
    }

    private async databases() {
        redis.on("ready", () => {
            log("Connected to Redis", "info", { type: "preset", color: "green" })
        })
    }

    private loadMethods() {
        String.prototype.removeIndents = function(this: string) {
            return this.replace(/^ */gm, "")
        }

        String.prototype.capitalizeFirstLetter = function(this: string) {
            return this[0].toUpperCase() + this.slice(1).toLowerCase()
        }
    }
}

export { client, Illegitimate, redis }
