import { createEnv } from "@t3-oss/env-core"
import { z } from "zod"
import { MissingEnvVarsError } from "./Classes/EnvVarError"

const prodEnv = createEnv({
    server: {
        TOKEN: z.string({ message: "TOKEN" }).min(1),
        DEV: z.string({ message: "DEV" }).min(1),
        HYPIXELAPIKEY: z.string({ message: "HYPIXELAPIKEY" }).min(1),
        REDISURI: z.string({ message: "REDISURI" }).min(1),
        POSTGRESURI: z.string({ message: "POSTGRESURI" }).min(1)
    },
    runtimeEnv: process.env,
    onValidationError: (e) => {
        const allErrors = e.map(err => err.message).join(" ")

        throw new MissingEnvVarsError(`[PROD]: ${allErrors}`)
    }
})

const devEnv = createEnv({
    server: {
        DEVTOKEN: z.string({ message: "DEVTOKEN" }).min(1),
        CLIENTID: z.string({ message: "CLIENTID" }).min(1),
        DEVID: z.string({ message: "DEVID" }).min(1),
        GUILDID: z.string({ message: "GUILDID" }).min(1)
    },
    runtimeEnv: process.env,
    skipValidation: process.env.NODE_ENV !== "dev",
    onValidationError: (e) => {
        const allErrors = e.map(err => err.message).join(" ")

        throw new MissingEnvVarsError(`[DEV]: ${allErrors}`)
    }
})

const env = {
    prod: prodEnv,
    dev: devEnv
}

export default env
