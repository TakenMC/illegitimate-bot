import axios from "axios"
import { IGuild, IGuildData, IPlayer, IPlayerData } from "~/typings"
import env from "~/utils/Env"
const apikey = env.prod.HYPIXELAPIKEY
const mojang = "https://api.mojang.com/users/profiles/minecraft/"
const mojanguuid = "https://sessionserver.mojang.com/session/minecraft/profile/"
const hypixel = "https://api.hypixel.net/v2/player"
const guild = "https://api.hypixel.net/v2/guild"
const minotar = "https://minotar.net/helm/"
type GuildQueryType = "player" | "name" | "id"

type UUIDData = {
    id: string
    name: string
}

type IGNData = {
    id: string
    name: string
    properties: { name: string, value: string }[]
    profileActions: []
}

async function getUUID(ign: string): Promise<string | null> {
    try {
        const req = await axios.get<UUIDData>(mojang + ign)
        return req.data.id
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (err) {
        return null
    }
}

async function getIGN(uuid: string): Promise<string | null> {
    try {
        const req = await axios.get<IGNData>(mojanguuid + uuid)
        return req.data.name
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (err) {
        return null
    }
}

async function getPlayer(uuid: string): Promise<IPlayerData | null> {
    try {
        const req = await axios.get<IPlayer>(hypixel, {
            params: {
                uuid: uuid
            },
            headers: {
                "API-Key": apikey
            }
        })
        if (!req.data.player) {
            return null
        }

        return req.data.player
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (err) {
        return null
    }
}

async function getGuild(query: string, reqType: GuildQueryType = "player"): Promise<IGuildData | null> {
    try {
        const req = await axios.get<IGuild>(guild, {
            params: {
                [reqType]: query
            },
            headers: {
                "API-Key": apikey
            }
        })

        if (!req.data.guild) {
            return null
        }

        return req.data.guild
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
    } catch (err) {
        return null
    }
}

function getHeadURL(ign: string): string {
    return minotar + ign
}

export { getGuild, getHeadURL, getIGN, getPlayer, getUUID }
