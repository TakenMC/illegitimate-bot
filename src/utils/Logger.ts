import chalk from "chalk"
import { logTimeFormatter } from "./Functions/intlFormaters"

const colors = {
    red: "#f38ba8",
    lavender: "#b4befe",
    green: "#a6e3a1",
    pink: "#f5c2e7"
}

type Colors = keyof typeof colors

const AllLogs = {
    info: { m: "INFO", c: "#a6e3a1" },
    warn: { m: "WARN", c: "#fab387" },
    error: { m: "ERROR", c: "#f38ba8" },
    critical: { m: "CRITICAL", c: "#ff3d3d" },
    debug: { m: "DEBUG", c: "#f9e2af" }
} as const

type LogType = keyof typeof AllLogs

type CustomColorProps = {
    type: "preset"
    color: Colors
} | {
    type: "custom"
    color: string
}

export function log(m: any, t: LogType, c?: CustomColorProps): void {
    const date = new Date()
    const time = logTimeFormatter(date)
    const logType = AllLogs[t].m
    const logColor = AllLogs[t].c

    const message = `${time} - [${logType}] | ${m}`
    if (!c) {
        console.log(cc(message, logColor))
        return
    }

    if (c.type === "preset") {
        const color = colors[c.color]
        console.log(cc(message, color))
    } else {
        console.log(cc(message, c.color))
    }
}

function cc(text: string, color: string) {
    return chalk.hex(color)(text)
}
