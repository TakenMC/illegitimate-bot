import { Events } from "discord.js"
import fs from "fs"
import path from "path"
import { embedColor } from "~/config/options"
import { IAutocomplete } from "~/typings"
import { ExtendedClient as Client } from "~/utils/Client"
import logToChannel from "~/utils/Functions/logtochannel"
import { log } from "../Logger"
type FileType = "js" | "ts"

export default async function loadAutocompleteEvents(client: Client, ft: FileType) {
    const autocompletePath = path.join(import.meta.dirname, "..", "..", "components", "autocomplete")
    const autocompleteFiles = fs.readdirSync(autocompletePath).filter(file => file.endsWith(ft))

    for (const file of autocompleteFiles) {
        const filePath = path.join(autocompletePath, file)
        const { default: autocomplete } = await import("file://" + filePath) as { default: IAutocomplete }
        client.autocomplete.set(autocomplete.name, autocomplete)
    }

    client.on(Events.InteractionCreate, async interaction => {
        if (!interaction.isAutocomplete()) return

        const autocomplete = client.autocomplete.get(interaction.commandName)

        if (!autocomplete) {
            log(`No autocomplete matching ${interaction.commandName} was found.`, "error")
            return
        }

        try {
            await autocomplete.execute({ interaction, client })
        } catch (error) {
            if (process.env.NODE_ENV !== "dev") {
                await logToChannel("error", {
                    embeds: [{
                        title: "Autocomplete error occured",
                        description: "```" + error + "```",
                        color: embedColor,
                        footer: {
                            icon_url: interaction.guild!.iconURL() || undefined,
                            text: interaction.user.username + " | " + interaction.commandName
                        }
                    }]
                })
            }
            log(error, "error")
        }
    })
}
