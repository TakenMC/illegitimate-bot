import { ExtendedClient as Client } from "../Client"
import loadAutocompleteEvents from "./autocomplete"
import loadButtonEvents from "./button"
import loadSlashCommandsEvents from "./command"
import loadContextMenuEvents from "./contextmenu"
import loadCronEvents from "./cron"
import loadEvents from "./events"
import loadModalEvents from "./modal"

export default async function loadAllEvents(client: Client, ft: "js" | "ts") {
    await loadEvents(client, ft)
    await loadButtonEvents(client, ft)
    await loadSlashCommandsEvents(client, ft)
    await loadContextMenuEvents(client, ft)
    await loadModalEvents(client, ft)
    await loadAutocompleteEvents(client, ft)
    await loadCronEvents(ft)
}
