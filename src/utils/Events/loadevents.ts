import { ExtendedClient as Client } from "../Client"
import { log } from "../Logger"
import loadAutocompleteEvents from "./autocomplete"
import loadButtonEvents from "./button"
import loadSlashCommandsEvents from "./command"
import loadContextMenuEvents from "./contextmenu"
import loadCronEvents from "./cron"
import loadEvents from "./events"
import loadModalEvents from "./modal"

export default async function loadAllEvents(client: Client, ft: "js" | "ts") {
    await loadEvents(client, ft).then(() => log("Events loaded", "info"))
    await loadButtonEvents(client, ft).then(() => log("Button events loaded", "info"))
    await loadSlashCommandsEvents(client, ft).then(() => log("Slash commands loaded", "info"))
    await loadContextMenuEvents(client, ft).then(() => log("Context menu events loaded", "info"))
    await loadModalEvents(client, ft).then(() => log("Modal events loaded", "info"))
    await loadAutocompleteEvents(client, ft).then(() => log("Autocomplete events loaded", "info"))
    await loadCronEvents(ft).then(() => log("Cron events loaded", "info"))
}
