import { Events, MessageFlags } from "discord.js"
import fs from "fs"
import path from "path"
import { embedColor } from "~/config/options"
import { IModal } from "~/typings"
import { ExtendedClient as Client } from "~/utils/Client"
import logToChannel from "~/utils/Functions/logtochannel"
import { log } from "../Logger"
type FileType = "js" | "ts"

export default async function loadModalEvents(client: Client, ft: FileType) {
    const modalPath = path.join(import.meta.dirname, "..", "..", "components", "modals")
    const modalFiles = fs.readdirSync(modalPath).filter(file => file.endsWith(ft))

    for (const file of modalFiles) {
        const filePath = path.join(modalPath, file)
        const { default: modal } = await import("file://" + filePath) as { default: IModal }
        client.modals.set(modal.name, modal)
    }

    client.on(Events.InteractionCreate, async interaction => {
        if (!interaction.isModalSubmit()) return

        const modal = client.modals.get(interaction.customId)

        if (!modal) {
            interaction.reply({
                content: "Modal logic not implemented. This is most likely an old modal",
                flags: MessageFlags.Ephemeral
            })
            log(`No modal matching ${interaction.customId} was found.`, "error")
            return
        }

        try {
            await modal.execute({ interaction, client })
        } catch (error) {
            if (process.env.NODE_ENV !== "dev") {
                await logToChannel("error", {
                    embeds: [{
                        title: "Button error occured",
                        description: "```" + error + "```",
                        color: embedColor,
                        footer: {
                            icon_url: interaction.guild!.iconURL() || undefined,
                            text: interaction.user.username + " | " + interaction.customId
                        }
                    }]
                })
            }

            log(error, "error")
            if (!interaction.deferred) {
                await interaction.reply({
                    embeds: [{
                        description: "There was an error while executing this modal!",
                        color: embedColor
                    }]
                })
            } else {
                await interaction.editReply({
                    embeds: [{
                        description: "There was an error while executing this modal!",
                        color: embedColor
                    }]
                })
            }
        }
    })
}
