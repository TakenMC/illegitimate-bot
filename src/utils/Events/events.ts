import fs from "fs"
import path from "path"
import { ExtendedClient as Client } from "~/utils/Client"
type FileType = "js" | "ts"

export default async function loadEvents(client: Client, ft: FileType) {
    const serverDir = path.join(import.meta.dirname, "..", "..", "events", "server")
    const eventDirs = fs.readdirSync(serverDir)
    for (const eventDir of eventDirs) {
        const eventFiles = fs.readdirSync(path.join(serverDir, eventDir)).filter(file => file.endsWith(ft))
        const eventName = eventDir
        for (const eventFile of eventFiles) {
            const eventPath = path.join(serverDir, eventDir, eventFile)
            const { default: event, once } = await import("file://" + eventPath)
            if (once && once === true) {
                client.once(eventName, event)
            } else {
                client.on(eventName, event)
            }
        }
    }
}
