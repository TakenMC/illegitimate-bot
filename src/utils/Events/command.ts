import { Events, MessageFlags } from "discord.js"
import fs from "fs"
import path from "path"
import { embedColor } from "~/config/options"
import { ICommand } from "~/typings"
import { ExtendedClient as Client } from "~/utils/Client"
import logToChannel from "~/utils/Functions/logtochannel"
import { log } from "../Logger"
type FileType = "js" | "ts"

export default async function loadSlashCommandsEvents(client: Client, ft: FileType) {
    const cmdPath = path.join(import.meta.dirname, "..", "..", "commands")
    const cmdFiles = fs.readdirSync(cmdPath).filter(file => file.endsWith(ft))

    for (const file of cmdFiles) {
        const filePath = path.join(cmdPath, file)
        const { default: cmd } = await import("file://" + filePath) as { default: ICommand }
        client.commands.set(cmd.data.name, cmd)
    }

    // ! command handler
    client.on(Events.InteractionCreate, async interaction => {
        if (!interaction.isChatInputCommand()) return

        const command = client.commands.get(interaction.commandName)

        if (!command) {
            interaction.reply({
                content: "Command logic not implemented. This is most likely an old command",
                flags: MessageFlags.Ephemeral
            })
            log(`No command matching ${interaction.commandName} was found.`, "error")
            return
        }

        try {
            await command.execute({ interaction, client })
        } catch (error) {
            if (process.env.NODE_ENV !== "dev") {
                await logToChannel("error", {
                    embeds: [{
                        title: "Command error occured",
                        description: "```" + error + "```",
                        color: embedColor,
                        footer: {
                            icon_url: interaction.guild!.iconURL() || undefined,
                            text: interaction.user.username + " | " + interaction.commandName
                        }
                    }]
                })
            }

            log(error, "error")
            if (!interaction.deferred) {
                await interaction.reply({
                    embeds: [{
                        description: "There was an error while executing this command!",
                        color: embedColor
                    }],
                    flags: MessageFlags.Ephemeral
                })
            } else {
                await interaction.editReply({
                    embeds: [{
                        description: "There was an error while executing this command!",
                        color: embedColor
                    }]
                })
            }
        }
    })
}
