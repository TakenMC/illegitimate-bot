import { CronJob } from "cron"
import fs from "fs"
import path from "path"
import { ICron } from "~/typings"
type FileType = "js" | "ts"

export default async function loadCronEvents(ft: FileType) {
    const cronPath = path.join(import.meta.dirname, "..", "..", "events", "cron")
    const cronFiles = fs.readdirSync(cronPath).filter(file => file.endsWith(ft))

    for (const file of cronFiles) {
        const filePath = path.join(cronPath, file)
        const { default: cron } = await import("file://" + filePath) as { default: ICron }
        const { seconds, minutes, hours, dayOfMonth, month, dayOfWeek } = cron.time

        const time = `${seconds} ${minutes} ${hours} ${dayOfMonth} ${month} ${dayOfWeek}`

        new CronJob(time, cron.execute, cron.onComplete, cron.start, cron.timeZone).start()
    }
}
