import { ApplicationCommandType, ContextMenuCommandBuilder, InteractionContextType, MessageFlags, PermissionFlagsBits, userMention } from "discord.js"
import { IContextMenu } from "~/typings"

export default {
    name: "congratsmessage",
    description: "Congratulate a user.",
    dev: false,

    data: new ContextMenuCommandBuilder()
        .setName("Congratulate")
        .setType(ApplicationCommandType.Message)
        .setContexts(InteractionContextType.Guild)
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageMessages),

    async execute({ interaction }) {
        const { targetId } = interaction
        const message = await interaction.channel!.messages.fetch(targetId)

        if (!message) {
            interaction.reply({
                content: "That user does not exist.",
                flags: MessageFlags.Ephemeral
            })
            return
        }

        const target = message.author

        await message.reply({
            embeds: [{
                title: "Congratulations!",
                description: `GG to ${userMention(target.id)}!`
            }]
        })
        await message.react("🎉")

        await interaction.reply({
            content: "Sent a congrats message",
            flags: MessageFlags.Ephemeral
        })
    }
} as IContextMenu
