import { ApplicationCommandType, ContextMenuCommandBuilder, InteractionContextType, MessageFlags, PermissionFlagsBits, userMention } from "discord.js"
import { getVerify } from "src/drizzle/functions"
import { devMessage, embedColor, hypixelGuildID } from "~/config/options"
import { waitingListRole } from "~/config/roles"
import { IContextMenu } from "~/typings"
import getGuildRank from "~/utils/Functions/guildrank"
import roleManage from "~/utils/Functions/rolesmanage"
import { getGuild, getHeadURL, getIGN } from "~/utils/Hypixel"

export default {
    name: "Update User",
    description: "Updates a user's roles",
    dev: false,

    data: new ContextMenuCommandBuilder()
        .setName("Update User")
        .setType(ApplicationCommandType.User)
        .setContexts(InteractionContextType.Guild)
        .setDefaultMemberPermissions(PermissionFlagsBits.Administrator),

    async execute({ interaction }) {
        await interaction.deferReply({ flags: MessageFlags.Ephemeral })

        const targetId = interaction.targetId
        const user = await interaction.guild!.members.fetch(targetId)
        const usermentioned = userMention(user.user.id)
        const verifyData = await getVerify({ userID: user.id })

        if (!verifyData) {
            await user.setNickname(`${user.user.username} (X)`, "User used the update command").catch(() => {
                // Do nothing
            })
            await interaction.editReply({
                embeds: [{
                    description: "User is not verified.\n\n" + "Updating username to `Username (X)`",
                    color: embedColor,
                    footer: {
                        text: interaction.guild!.name + " | " + devMessage,
                        icon_url: interaction.guild!.iconURL() || undefined
                    }
                }]
            })
            return
        }

        await interaction.editReply({
            embeds: [{
                description: "Fetching ign...",
                color: embedColor
            }]
        })

        const ign = (await getIGN(verifyData.uuid)) as string
        const head = getHeadURL(ign)

        await interaction.editReply({
            embeds: [{
                description: "Fetching guild data...",
                color: embedColor
            }]
        })

        const guild = await getGuild(verifyData.uuid)

        let responseGuildID: string | null
        if (!guild) {
            responseGuildID = null
        } else {
            responseGuildID = guild._id
        }

        if (responseGuildID !== hypixelGuildID) {
            const roles = roleManage("default")
            await user.roles.remove(roles.rolesToRemove, "User was force updated.")
            await user.roles.add(roles.rolesToAdd, "User was force updated.")
            await user.setNickname(ign, "User was force updated.").catch(() => {
                // Do nothing
            })

            await interaction.editReply({
                embeds: [{
                    description: `
                    ${usermentioned} was given the \`Default Member\` role.

                    IGN: \`${ign}\`
                    `,
                    color: embedColor,
                    thumbnail: {
                        url: head!
                    },
                    footer: {
                        text: interaction.guild!.name + " | " + devMessage,
                        icon_url: interaction.guild!.iconURL() || undefined
                    }
                }]
            })
            return
        }

        if (responseGuildID === hypixelGuildID) {
            const GuildMembers = guild!.members
            const guildRank = GuildMembers.find(member => member.uuid === verifyData.uuid)!.rank
            let replyRank: string | null = null

            const rank = getGuildRank(guildRank)
            if (rank) {
                await user.roles.remove(rank.rolesToRemove, "User was force updated.")
                await user.roles.add(rank.rolesToAdd, "User was force updated.")
                replyRank = rank.rank
            }

            await user.roles.remove(waitingListRole, "User was force updated.")
            await user.roles.add(roleManage("default").rolesToAdd, "User was force updated.")

            await user.setNickname(ign, "User was force updated.").catch(() => {
                // Do nothing
            })

            await interaction.editReply({
                embeds: [{
                    description: `
                    ${usermentioned} was given the \`${replyRank}\` role.
                    
                    IGN: \`${ign}\`
                    `,
                    color: embedColor,
                    thumbnail: {
                        url: head!
                    },
                    footer: {
                        text: interaction.guild!.name + " | " + devMessage,
                        icon_url: interaction.guild!.iconURL() || undefined
                    }
                }]
            })
        }
    }
} as IContextMenu
