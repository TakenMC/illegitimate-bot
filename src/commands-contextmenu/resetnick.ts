import { ApplicationCommandType, ContextMenuCommandBuilder, InteractionContextType, MessageFlags, PermissionFlagsBits } from "discord.js"
import { IContextMenu } from "~/typings"

export default {
    name: "resetnick",
    description: "Reset your nickname.",
    dev: false,

    data: new ContextMenuCommandBuilder()
        .setName("Reset Nickname")
        .setType(ApplicationCommandType.User)
        .setContexts(InteractionContextType.Guild)
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageNicknames),

    async execute({ interaction }) {
        const { targetId } = interaction
        const target = await interaction.guild!.members.fetch(targetId)

        if (!target) {
            interaction.reply({
                content: "That user does not exist.",
                flags: MessageFlags.Ephemeral
            })
            return
        }

        if (target.id === interaction.user.id) {
            interaction.reply({
                content: "You can't reset your own nickname.",
                flags: MessageFlags.Ephemeral
            })
            return
        }

        if (!target.manageable) {
            interaction.reply({
                content: "I cannot reset that user's nickname.",
                flags: MessageFlags.Ephemeral
            })
            return
        }

        await target.setNickname(
            target.user.username,
            "Reset by " + interaction.user.username
        )
        interaction.reply({
            content: `Reset ${target.user.username}'s nickname.`,
            flags: MessageFlags.Ephemeral
        })
    }
} as IContextMenu
