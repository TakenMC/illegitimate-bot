import { REST, Routes } from "discord.js"
import env from "../src/utils/Env"
import { log } from "../src/utils/Logger"

const rest = new REST({ version: "10" }).setToken(env.dev.DEVTOKEN)

try {
    log("Started deleting application (/) commands.", "info")
    await rest.put(
        Routes.applicationGuildCommands(env.dev.DEVID, env.dev.GUILDID),
        { body: [] }
    )
    log("Successfully deleted application (/) commands.", "info")
} catch (error) {
    console.error(error)
}
