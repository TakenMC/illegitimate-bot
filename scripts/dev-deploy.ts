import { REST, RESTPutAPIApplicationCommandsJSONBody, Routes } from "discord.js"
import fs from "fs"
import { ICommand } from "../src/typings"
import env from "../src/utils/Env"
import { log } from "../src/utils/Logger"
const rest = new REST({ version: "10" }).setToken(env.dev.DEVTOKEN)

const commands: RESTPutAPIApplicationCommandsJSONBody = []
const commandFiles = fs.readdirSync("./src/commands/").filter(file => file.endsWith(".ts"))
const contentMenuCommands = fs.readdirSync("./src/commands-contextmenu/").filter(file => file.endsWith(".ts"))

for (const file of commandFiles) {
    const { default: command } = await import(`../src/commands/${file}`) as { default: ICommand }
    if (command.dev) {
        commands.push(command.data.toJSON())
    }
}
for (const file of contentMenuCommands) {
    const { default: command } = await import(`../src/commands-contextmenu/${file}`) as { default: ICommand }
    if (command.dev) {
        commands.push(command.data.toJSON())
    }
}

try {
    log(`Started refreshing ${commands.length} application (/) commands.`, "info")

    await rest.put(
        Routes.applicationGuildCommands(env.dev.DEVID, env.dev.GUILDID),
        { body: commands }
    ).then(() => {
        log(`Successfully reloaded ${commands.length} application (/) commands.`, "info")
        process.exit(0)
    })
} catch (error) {
    console.error(error)
}
