import { REST, RESTPutAPIApplicationCommandsJSONBody, Routes } from "discord.js"
import fs from "node:fs"
import { ICommand } from "../src/typings"
import env from "../src/utils/Env"
import { log } from "../src/utils/Logger"
const rest = new REST({ version: "10" }).setToken(env.prod.TOKEN)

const commands: RESTPutAPIApplicationCommandsJSONBody = []
const commandFiles = fs.readdirSync("./src/commands").filter(file => file.endsWith(".ts"))
const contentMenuCommands = fs.readdirSync("./src/commands-contextmenu").filter(file => file.endsWith(".ts"))

for (const file of commandFiles) {
    const { default: command } = await import(`../src/commands/${file}`) as { default: ICommand }
    commands.push(command.data.toJSON())
}
for (const file of contentMenuCommands) {
    const { default: command } = await import(`../src/commands-contextmenu/${file}`) as { default: ICommand }
    commands.push(command.data.toJSON())
}

try {
    log(`Started refreshing ${commands.length} application (/) commands.`, "info", { type: "preset", color: "green" })

    const commandsString = commands.map(command => " " + command.name)
    log(commandsString.join("\n"), "info", { type: "preset", color: "lavender" })

    await rest.put(
        Routes.applicationCommands(env.dev.CLIENTID),
        { body: commands }
    ).then(() => {
        log(`Successfully reloaded ${commands.length} application (/) commands.`, "info", { type: "preset", color: "green" })
        process.exit(0)
    })
} catch (error) {
    console.error(error)
}
